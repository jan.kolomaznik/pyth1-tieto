Python - základy programování
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Python je jednoduchý a zároveň mocný programovací jazyk. 
Přestože se velmi lehce se učí, využívá se v různých oblastech - od zpracování vědeckých dat, přes zpracování obrazu až k výrobě webových stránek.
Pomocí Pythonu můžete zautomatizovat pracné části své práce a tím si zjednodušit život.
V rámci školení se seznámíte s fungováním jazyka Python, podíváte se na složitější příkazy a možnostmi hledání a využití standardní knihovny. 
Součástí kurzu jsou samozřejmě také praktická cvičení, během nichž mimo jiné vyzkoušíte vytvoření automatizace dvou úkolů. 

Náplň kurzu:
------------

- **[Třídy a objekty](00-python.object.ipynb)**
    - Základní pojmy OOP
    - OOP v Pythonu
    - Dědičnost
    - Polymorfismus
    - Skládání
    - Magické metody
    - Třída - defaultní atributy
    - Dokumentace třídy
    - Zjišťování typu
    * Definice tříd
    * Objekty
    * Atributy
- **[Řetězce a regulární výrazy](01-python.string.re.ipynb)**
- **[duly](02-python.module.ipynb)**
    * Seznámení s moduly
    * Přehled často využívaných balíčků
    * Vytváření vlastních modulů a balíčků
- **[Zpracování dat](03-python.file.ipynb)**
    - Mody otevření soubotu
    * Práce se soubory a adresáři
- **Skripty**
    * Tvorba jednoduchého skriptu
    * Argumenty
- **Distribuce a testování**
    - **[Testování](04-python.unittest.ipynb)**
        - Instalace knihovny pytest
        - Psaní testů
        - Spouštění testů
        - Testovací moduly
        - Spouštěcí moduly
        - Pozitivní a negativní testy
    - **[stování](05-python.pytest.ipynb)**
- **Diskuse, dotazy**
    * Tvorba čitelného kódu
- **[Základní konstrukce jazyka](06-python.construction.comprehensions.ipynb)**
- **[Lambdas](07-python.function.lambdas.ipynb)**
- **[Dekorátory](08-python.decorator.ipynb)**
- **[nerátory](09-python.genarators.ipynb)**
- **[Itertools](10-python.itertools.ipynb)**
    - Co je to Itertools
    - Proč Itertools používat?
    - Funkce `itertools.zip_longest()`
    - Et tu, Brute Force?
    - Sekvence čísel
    - Rekurze
    - Karetní příklad
    - Odbočení: Práce se seznam seznamů
    - Filtrování
    - Seskupení
- **[Functools](11-python.functools.ipynb)**
    - Partial functions
    - Total ordering pomocí functools
    - Funkce `reduce()`
    - Dekorátor funkce `@lru_cache`
    - Další funkce
- **[First-Class Objects](12-python.object.first-class.ipynb)**
    - Funkce jsou objekty
    - Funkce v datových strukturách
    - Práce u kazateli na funkce
    - Vnořené funkce
    - Funkce mohou zachytit místní stav
    - Objekty jako funkce (Functors)
    - Shrnutí
- **[tps://realpython.com/python-data-classes](13-python.dataclass.ipynb)**

Práce se soubory (pickle, shelve), 
databáze, 
ORM (SQL Alchemy)

Síťová komunikace, 
Properties vs. gettry a settry, 
Map (reduce, filter), 



