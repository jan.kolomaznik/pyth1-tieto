Python - základy programování
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Úvod do jazyka Python. 
V tomto uvodud se seznáníme se základy jazyka, jeho syntaxí, základními datovými typy a operacemi, které nám poskytují.
Také se podíváme na konstrukce pro řízení toku programu, jako jsou cykly a větvení.
Následně se naučíme definovat funkce a používat.
Poslední dvě témata jsou venována zpracování výjimek a prací s řetězci.

 
Náplň dne:
----------

- **[Seznámení s jazykem Python](00-python.ipynb)**
    - Charakteristika
    - K čemu se python hodí?
    - Instalace
    - Možnosti vývoje
  * Variace (PyPy, Cython, Jython, RPython, Stackless Python, IronPython), 
- **[Základy jazyka](01-python.basic.ipynb)**
    - Komentáře
    - Vystup konsole (`print()`)
    - Proměnné
    - Vstup z konsole (`input()`)
    - Pravidla pro pojmenování proměnných
    - Operátory přiřazení
    - Rezervovaná slova
    - Konce řádků a středníky
    - Import funkci, objeku, modulu
- **[Datové typy](02-python.type.ipynb)**
    - Základní datové typy
    - Prázdná hodnota
    - Logické hodnoty (bool)
    - Číselné datové typy
    - Řetězce (str)
    - Řetězec jako seznam
    - Konverze datových typů
- **[Základní datové struktury](03-python.structure.ipynb)**
    - N-tice (tuple)
    - Seznam (list)
    - Množina (set)
    - Slovníky (dict)
- **[Základní konstrukce jazyka](04-python.construction.ipynb)**
    - Podmínka (`if`)
    - Smyčka (`for`)
    - Smyčka (`while`)
- **[Funkce](05-python.function.ipynb)**
    - Výchozí hodnoty argumentů
    - Proměnný počat argumentů
    - Dokumentace funkce
    - Ukazatele na funkce a návratové typy
- **[Chyby a výjimky](06-python.except.ipynb)**
    - Zachycení a ošetření výjimek
    - Kontext výjimek
    - Vyvolání výjimky
- **[Řetězce](07-python.string.ipynb)**
    - Formátování řetězce
    - Metody
    * Vyhledánání v řetězci
    * Uprava řetězců
    - **[Řetězce a regulární výrazy](08-python.string.re.ipynb)**
 